<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\video;


class chartReviewController extends Controller
{
    private $video;

    public function __construct() {
        $this->video = new video();
    }

    public function fillView(Request $request) {
        $movieQuery = $this->video->query();
        $movieQuery->VideosListId($request->session()->get('carrito'));
        $totalPrice = $movieQuery->get()->sum("price");

        $moviesWantsToBuyQuery = $this->video->query();
        $moviesWantsToBuyQuery->VideosListId($request->session()->get('carrito'));
        $moviesWantsToBuy = $moviesWantsToBuyQuery->get();

        $operations =  $request->session()->get('operations');

        //dd($request->session()->get('carrito'));
        
        if ($request->session()->get('stage') == 0 || $request->session()->get('stage') == 1) {
            $request->session()->put("stage",1);
            return view("chartReview")->with(['movies' => $moviesWantsToBuy ])->with(['totalPrice' => $totalPrice ])->with(['operations' => $operations]);
        } elseif ($request->session()->get('stage') == 2) {
            return view("register");
        }
    }
}
