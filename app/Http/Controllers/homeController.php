<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\video;
use App\Models\genre;

class homeController extends Controller
{
    private $video;
    private $genre;

    public function __construct() {
        $this->video = new video();
        $this->genre = new genre();
    }


    public function fillHome(Request $request) {
        $querylistGenres =  $this->genre->query();
        $querylistGenres->ListGenres();
        $listGenres = $querylistGenres->get();

        $movieImagesQuery = $this->video->query();
        $movieImagesQuery->All();
        $movieImages = $movieImagesQuery->get();

        return view("home")->with(['listGenres' => $listGenres ])->with(['movies' => $movieImages ]);
    }
}
