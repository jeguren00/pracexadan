<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\video;


class orderController extends Controller
{
    private $video;

    public function __construct() {
        $this->video = new video();
    }


    public function addToChart(Request $request, $IdVid, $operation)
    {

        $carrito = $request->session()->get('carrito', []);
        $operations = $request->session()->get('operations', []);

        array_push($carrito,$IdVid);
        array_push($operations,$IdVid." ".$operation);
        $request->session()->put("stage",0);
        $request->session()->put('carrito', $carrito);
        $request->session()->put('operations', $operations);

        //Redirect to page who send the request:
        return redirect(url()->previous());
    }

    public function emptyChart(Request $request){
        $request->session()->forget('carrito');
        $request->session()->forget('operations');
        $request->session()->put("stage",0);
        return redirect(url()->previous());
    }
}
