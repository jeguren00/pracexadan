<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\registerRequest;

class registerController extends Controller
{
    //

    public function registerView(Request $request) {
        $request->session()->put("stage",2);
        return view('register');
    }

    public function registerSave(registerRequest $request) {
        $request->session()->put("stage",0);
        return redirect()->route('/');
    }
}
