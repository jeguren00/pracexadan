<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\video;
use App\Models\genre;

class searchClass extends Controller
{
    private $video;

    public function __construct() {
        $this->video = new video();
    }


    public function searchByGen($idGen) {
        $queryListVideoByGen =  $this->video->query();
        $queryListVideoByGen->VideoByGen($idGen);
        $listMoviesByGen = $queryListVideoByGen->get();

        return view("searchResult")->with(['movies' => $listMoviesByGen ]);
    }

    public function searchByTitle(Request $request) {
        $VideoByTitle = $request->input('searchText');

        $queryListVideoTitle =  $this->video->query();
        $queryListVideoTitle->VideoByTitle($VideoByTitle);
        $listMoviesByTitle = $queryListVideoTitle->get();

        return view("searchResult")->with(['movies' => $listMoviesByTitle ]);
    }
}
