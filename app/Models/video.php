<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class video extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Videos';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idVideo';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'double',
        
    ];

    public function scopeAll($query){
        return $query;
    }

    public function scopeVideosListId($query, array $idVids){
        return $query->whereIn('idVideo', $idVids);
    }

    public function scopeVideoByGen($query, $idgen){
        return $query->rightjoin('GenresXVideos', 'Videos.idVideo', '=', 'GenresXVideos.idVideo')->where('idGenere',$idgen);
    }

    public function scopeVideoByTitle($query, $title){
        return $query->rightjoin('GenresXVideos', 'Videos.idVideo', '=', 'GenresXVideos.idVideo')->where('title', 'like', '%'.$title.'%')->distinct()->count('Videos.idVideo');
    }

    public function scopeOneVideo($query, $idVid){
        return $query->where('idVideo',$idVid);
    }
}
