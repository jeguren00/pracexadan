<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenresXVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GenresXVideos', function(Blueprint $table){
            $table->unsignedInteger('idGenere');
            $table->unsignedInteger('idVideo');
            $table->foreign('idVideo')->references('idVideo')->on('Videos');
            $table->foreign('idGenere')->references('idGenere')->on('Genres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GenresXVideos');
    }
}
