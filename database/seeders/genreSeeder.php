<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class genreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Genres')->insert(
            ['name' => 'historic']
        );

        DB::table('Genres')->insert(
            ['name' => 'belic']
        );

        DB::table('Genres')->insert(
            ['name' => 'science fiction']
        );

    }
}
