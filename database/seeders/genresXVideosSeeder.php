<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class genresXVideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('GenresXVideos')->insert(
            ['idGenere' => '3', 
            'idVideo' => '1'
            ]
        );

        DB::table('GenresXVideos')->insert(
            ['idGenere' => '1', 
            'idVideo' => '2'
            ]
        );

        DB::table('GenresXVideos')->insert(
            ['idGenere' => '2', 
            'idVideo' => '2'
            ]
        );

        DB::table('GenresXVideos')->insert(
            ['idGenere' => '2', 
            'idVideo' => '3'
            ]
        );
    }
}
