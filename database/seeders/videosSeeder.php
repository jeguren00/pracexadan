<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class videosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //science fiction
        DB::table('Videos')->insert(
            [
                'title'     => 'Back to the future', 
                'image'     => "./img/backToTheFuture.jpg" ,
                'price'      => 10.12
            ]
        );
        //belic and historic
        DB::table('Videos')->insert(
            [
                'title'     => 'A bridge too far', 
                'image'     => "./img/aBridgeTooFar.jpg" ,
                'price'      => 15.22
            ]
        );
        //only belic
        DB::table('Videos')->insert(
            [
                'title'     => 'The longuest day', 
                'image'     => "./img/theLonguestDay.jpg" ,
                'price'      => 10.50
            ]
        );
    }
}
