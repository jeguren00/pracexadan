@extends('layouts/layouts')

@section('content')
    <div class="row">
        <div class="col">
            <h2>Final check</h2>
            <p>Total productos: {{sizeof(app('request')->session()->get('carrito',[]))}}</p>
            <p>Precio Total: {{ $totalPrice }}</p>

        </div>

        <div class="col">
            <div class="container mt-5">
                <div class="row">
                <h1 id="peliculas">Películas</h1>
                    @forelse ($movies as $peli)
                        <div class="col col-4 d-flex flex-wrap mt-5">
                            <div class="card" style="width: 18rem;">
                                <img class="card-img-top" src="{{ asset($peli->image) }}" alt="Card image cap">
                                <div class="card-body">
                                    <h3 class="card-title">{{ $peli->title }}</h3>
                                    @for ($i = 0; $i < count($operations); $i++)
                                        @if (trim(substr($operations[$i],0,strpos($operations[$i], " "))) == $peli->idVideo)
                                            <p>{{ substr($operations[$i],strpos($operations[$i], " "))  }}</p>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No hay películas</p>
                    @endforelse
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/" class="btn btn-secondary">Atras</a>
        </div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>

        <div class="col">
            <a href="/register" class="btn btn-primary">Siguiente</a>
        </div>
    </div>

@endsection