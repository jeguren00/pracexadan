@extends('layouts/layouts')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="container">
                <form action="/search">
                    <h2>Buscar:</h2>
                    <input type="text" name="searchText"></input>
                </form>
            </div>
            <div class="container">
                <ul class="list-group">
                    @forelse ($listGenres as $gen)
                        <li class="list-group-item"><a class="nav-link" href="/search/{{ $gen->idGenere }}">{{ $gen->name }}</a></li>
                    @empty
                        <p>sin generos</p>
                    @endforelse
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    @forelse ($movies as $img)
                        @if ($img->idVideo == 1)
                            <div class="carousel-item active">
                        @else
                            <div class="carousel-item">
                        @endif
                            <img src="{{ asset($img->image) }}" style="width: 100%;">
                        </div>
                    @empty
                        <p>sin imagenes</p>
                    @endforelse
                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <h1 id="peliculas">Películas</h1>
            @forelse ($movies as $peli)
                <div class="col col-4 d-flex flex-wrap mt-5">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{ asset($peli->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <h3 class="card-title">{{ $peli->title }}</h3>
                            <div>
                                <a href="/addCarrito/{{ $peli->idVideo }}/Alquilar" class="btn btn-primary">Alquilar</a>
                                <a href="/addCarrito/{{ $peli->idVideo }}/Comprar" class="btn btn-primary">Comparar</a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p>No hay películas</p>
            @endforelse
        </div>
    </div>
@endsection