<html>
    <head>
        <title>Neflis</title>
        <meta charset="utf-8">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </head>

    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="/">Neflis</a>
        <ul class="nav nav-stacked">
            <li class="nav-item"><a class="nav-link text-white">Stage: {{app('request')->session()->get('stage')}}</a></li>
            <li class="nav-item"><a class="nav-link text-white">Numero de productos: {{sizeof(app('request')->session()->get('carrito',[]))}}</a></li>
            @if (sizeof(app('request')->session()->get('carrito',[])) != 0)
                <li class="nav-item"><a href="/emptyChart" class="nav-link text-white">Borrar Carrito</a></li>
            @endif        
            <li class="nav-item"><a href="/reviewChart" class="nav-link text-white">Finalizar compra</a></li>
        </ul>
    </nav>
@yield('content')