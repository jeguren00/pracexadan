@extends('layouts/layouts')

@section('content')
    <form method="POST" action="/register/save">
        @csrf
        <div class="form-group">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="name" placeholder="Pepe" />
                <label for="nombre">Name</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="surname" placeholder="Pepe" />
                <label for="apellidos">E-Mail Adress:</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="adress" placeholder="Pepe" />
                <label for="username">Direccion:</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-floating mb-3">
                <input type="password" class="form-control" name="password" placeholder="Pepe" />
                <label for="username">Password:</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-floating mb-3">
                <input type="password" class="form-control" name="passwordconf" placeholder="Pepe" />
                <label for="username">Confirm Password:</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-floating mb-3">
                <input type="file" class="form-control" name="photo" placeholder="Pepe" />
                <label for="username">Foto:</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


@endsection