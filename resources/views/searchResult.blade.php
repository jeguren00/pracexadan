@extends('layouts/layouts')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <h1 id="peliculas">Películas</h1>
            @forelse ($movies as $peli)
                <div class="col col-4 d-flex flex-wrap mt-5">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{ asset($peli->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <h3 class="card-title">{{ $peli->title }}</h3>
                            <div>
                                <a href="/addCarrito/{{ $peli->idVideo }}/Alquilar" class="btn btn-primary">Alquilar</a>
                                <a href="/addCarrito/{{ $peli->idVideo }}/Comprar" class="btn btn-primary">Comparar</a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p>No hay películas</p>
            @endforelse
        </div>
    </div>

@endsection