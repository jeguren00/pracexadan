<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\searchClass;
use App\Http\Controllers\orderController;
use App\Http\Controllers\chartReviewController;
use App\Http\Controllers\registerController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('home');
});
*/
Route::get('/', [homeController::class,'fillHome']);

Route::get('/search/{idGen}', [searchClass::class,'searchByGen']);
Route::get('/search', [searchClass::class,'searchByTitle']);
Route::get('/addCarrito/{IdVid}/{operation}', [orderController::class,'addToChart']);
Route::get('/emptyChart', [orderController::class,'emptyChart']);
Route::get('/reviewChart', [chartReviewController::class,'fillView']);
Route::post('/register/save', [registerController::class,'registerSave']);
Route::get('/register', [registerController::class,'registerView']);

